CREATE TABLE restorante (
    restorante_id serial PRIMARY KEY,
    restorante_name varchar (80) NOT NULL
);


CREATE TABLE tisch (
    tisch_id serial,
    tisch_nummer serial NOT NULL,
    restorante_id bigint NOT NULL,
    PRIMARY KEY(tisch_id),
   CONSTRAINT fk_restorante
      FOREIGN KEY(restorante_id)
	  REFERENCES restorante(restorante_id)
);

CREATE TABLE gueste (
    gueste_id serial PRIMARY KEY,
    passwort varchar (80) NOT NULL,
    gueste_nummer serial NOT NULL
);

CREATE TABLE anwehsrenheitsdaten (
  anwehsrenheitsdaten_id  serial ,
  datum_von timestamp  NOT NULL ,
  datum_bis timestamp  NOT NULL ,
  tisch_id serial NOT NULL  ,
  gueste_id serial NOT NULL,
    PRIMARY KEY (anwehsrenheitsdaten_id),
   CONSTRAINT fk_tisch
      FOREIGN KEY(tisch_id)
	  REFERENCES tisch(tisch_id),

  CONSTRAINT fk_gueste
      FOREIGN KEY(gueste_id)
	  REFERENCES gueste(gueste_id)
);
